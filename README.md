# C# OpenFolderDialog example supporting WebDAV

This is a simple C Sharp example using a graphical user interface to display a **Folder Selection Dialog** with WebDAV support.

The network mapped webdav drive should be done separately on Windows, this application doesn't configure it for you.

Even thou you can use this repo to test the intended functionality, there's a `Creating a New App` section below that shows how it was done from scratch.

## Running the app

- Open `cmd.exe` or **Windows PowerShell** and run the comands below:
```Bash
cd PROJECT_FOLDER
dotnet run --project SimpleWinForms\SimpleWinForms.csproj
```
_*_  P.s.: replace `PROJECT_FOLDER` with your project working directory. i.e. `C:\myproj\`

- Select the webdav folder and hit OK.
- Check the app window title.

## Troubleshooting

### Path Env var not properly defined.

If you're using **Windows PowerShell** and you've received the error below:
```
dotnet : The term 'dotnet' is not recognized as the name of a cmdlet, function, script file, or operable program. Check the spelling of the name, or if a path was included, verify that the path is correct and try again.
At line:1 char:1
+ dotnet
+ ~~~~~~
    + CategoryInfo          : ObjectNotFound: (dotnet:String) [], CommandNotFoundException
    + FullyQualifiedErrorId : CommandNotFoundException
```

Or if you're using **cmd** and you've received the error below:
```
'dotnet' is not recognized as an internal or external command,
operable program or batch file.
```

It means that you either don't have .NET SDK installed or it's not properly set in your Env var, one quick solution it's to use the dotnet full path instead of just typing `dotnet`.
It's usually installed at: `C:\Program Files\dotnet\dotnet`.

## Creating a New App

- Open `cmd` or **Windows Powershell** and type the commands below:

```cmd
md SimpleWinForms
cd SimpleWinForms
dotnet new winforms -n SimpleWinForms
dotnet new sln
dotnet sln add SimpleWinForms
```
- Using your favorite text editor (I'm using VS Code) open `.\SimpleWinForms\Form1.Designer.cs`.
- Right below the line `private System.ComponentModel.IContainer components = null;`, add `private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;` (object instance)
- Right below `this.Text = "Form1;` add the lines below:
```CSharp
        this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
        DialogResult result = folderBrowserDialog1.ShowDialog();
        if (result == DialogResult.OK) {
            this.Text = this.folderBrowserDialog1.SelectedPath;
        }
```
- Run the app and select a webdav folder, the Title of the window will display the selected path. 

## Environment used (OS / Softwares)

- Windows 10 Professional version 21H2 (OS Build 19044.1466 - 64bit). Will work on Windows 8.1 or newer
- [.NET Framework SDK v6.0.101 (x64)](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/sdk-6.0.101-windows-x64-installer?journey=vs-code)
- Visual Code (VS Code)

## Credentials on a WebDAV server

If you don't have a WebDAV server nor you don't want to waste time configuring one, use the credentials below:

- Address: `https://pg.scherma.com.br:5006/contentstudio/`
- Username: `contentstudioteam`
- Password: `cStud1o@2022`

## References
- [C-Sharp Corner: FolderBrowserDialog in C#](https://www.c-sharpcorner.com/uploadfile/mahesh/folderbrowserdialog-in-C-Sharp/)
- [Github: Windows Forms .NET Core Designer](https://github.com/dotnet/winforms/blob/main/docs/winforms-designer.md)
