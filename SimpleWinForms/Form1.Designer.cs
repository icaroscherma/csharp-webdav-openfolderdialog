﻿namespace SimpleWinForms;

partial class Form1
{
    private System.ComponentModel.IContainer components = null;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1; // Important line

    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null)) {
            components.Dispose();
        }
        base.Dispose(disposing);
    }
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(800, 450);
        this.Text = "You havent selected a folder.";

        // Important part =)
        this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
        DialogResult result = folderBrowserDialog1.ShowDialog();
        if (result == DialogResult.OK) {
            this.Text = this.folderBrowserDialog1.SelectedPath;
        }
    }
}
